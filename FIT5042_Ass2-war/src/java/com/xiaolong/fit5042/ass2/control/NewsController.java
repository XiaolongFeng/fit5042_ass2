/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.control;

import com.xiaolong.fit5042.ass2.ejb.NewsBean;
import com.xiaolong.fit5042.ass2.entities.News;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Xiaolong
 */
@ManagedBean
@SessionScoped
public class NewsController implements Serializable{
    @EJB
    NewsBean newsBean;
    
    private News news;
    
    public NewsController() {}
    
    public News getNews()
    {
        return news;
    }
    
    public String read(News news)
    {
        this.news = news;
        return "news";
    }
    
    public String setupCreate()
    {
        news = new News();
        return "news";
    }
    
    public String create()
    {
        newsBean.createNews(news);
        return "index";
    }
    
    public String update()
    {
        newsBean.updateNews(news);
        return "index";
    }
    
    public String delete(News news)
    {
        newsBean.deleteNews(news);
        return "index";
    }
    
    public List<News> getNewsList()
    {
        return newsBean.findAllNews();
    }
    //return at most 5 news
    public List<News> getLatestNews()
    {
        ArrayList<News> latestNews = new ArrayList<>();
        List<News> newsList = newsBean.findAllNews();
        int size = newsList.size();
        if(size >=5 )
        {
            for(int x = 1; x <=5 ; x++)
            {
                latestNews.add(newsList.get(size-x));
            }
        }
        else
        {
            for(int x = size-1; x>=0 ; x--)
            {
                latestNews.add(newsList.get(x));
            }
        }      
        return latestNews;
    }
    
}
