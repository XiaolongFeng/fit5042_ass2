/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.control;

import com.xiaolong.fit5042.ass2.ejb.BookBean;
import com.xiaolong.fit5042.ass2.entities.Book;
import com.xiaolong.fit5042.ass2.entities.Comments;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Xiaolong
 */
@ManagedBean
@SessionScoped
public class BooksController implements Serializable {

    @EJB
    BookBean bookBean;
    
    private Book book;
    private String searchTag;
    private List<Book> searchBookList;
    
    
    public BooksController() {}

    public List<Book> getSearchBookList() {
        return searchBookList;
    }

    public void setSearchBookList(List<Book> searchBookList) {
        this.searchBookList = searchBookList;
    }
    
    public String getSearchTag() {
        return searchTag;
    }

    public void setSearchTag(String searchTag) {
        this.searchTag = searchTag;
    }
    
    public Book getBook()
    {
        return book;
    }
    
    public String read(Book book)
    {
        this.book = book;
        return "book";
    }
    
    public String setupCreate()
    {
        book = new Book();
        return "book";
    }
    
    public String create()
    {
        bookBean.createBook(book);
        return "index";
    }
    
    public String update()
    {
        bookBean.updateBook(book);
        return "index";
    }
    
    public String delete(Book news)
    {
        bookBean.deleteBook(news);
        return "index";
    }
    
    public List<Book> getBookList()
    {
        return bookBean.findAllBook();
    }
    //return at most 5 news
    public List<Book> getLatestBooks()
    {
        ArrayList<Book> latestBook = new ArrayList<>();
        List<Book> bookList = bookBean.findAllBook();
        int size = bookList.size();
        if(size >=3 )
        {
            for(int x = 1; x <=3 ; x++)
            {
                latestBook.add(bookList.get(size-x));
            }
        }
        else
        {
            for(int x = size-1; x>=0 ; x--)
            {
                latestBook.add(bookList.get(x));
            }
        }      
        return latestBook;
    } 
    
    public String searchBooksByTitle()
    {
        bookBean.setSearchTitle(searchTag.toLowerCase().trim());
        searchBookList = bookBean.findByTitle();
        return "booklist";
    }
    public String allComments()
    {
        String result = "";
        Collection<Comments> com = book.getCommentList();
        if(com.size() > 0){
            for(Comments c: com)
            {
                result += c.getUserid().getUserName();
                result += ": /n";
                result += c.getMessage();
                result += "/n/n";
            }
        }
        else
        {
            result = "There is no comment for this book...\n Write one!";           
        }
        return result;
    }
}
