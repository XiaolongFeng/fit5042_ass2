/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.Book;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class BookBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;
    
    private String searchTitle;

    public String getSearchTitle() {
        return searchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle.toLowerCase();
    }

    public void persist(Book book) {
        em.persist(book);
    }
    
    public void createBook(Book book)
    {
        em.persist(book);
    }

    public void updateBook(Book book)
    {
        em.merge(book);
    }
    
    public void deleteBook(Book book)
    {
        em.remove(em.merge(book));
    }
    
    public List<Book> findAllBook()
    {
        TypedQuery<Book> bookList = em.createQuery("SELECT n FROM Book n", Book.class);
        return bookList.getResultList();
    }
    
    public List<Book> findByTitle()
    {
        List<Book> allBooks = findAllBook();
        ArrayList<Book> bookList = new ArrayList<>();
        for(Book bk: allBooks)
        {
            if(bk.getTitle().toLowerCase().trim().contains(searchTitle))
                bookList.add(bk);
        }
        return bookList;
    }
    
    public Book findByID(Long id)
    {
        Book result = new Book();
        try{
            result = em.find(Book.class, id);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return result;
    }
    
    
}
