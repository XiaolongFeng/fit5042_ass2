/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.News;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class NewsBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(News news) {
        em.persist(news);
    }
    
    public void createNews(News news)
    {
        em.persist(news);
    }

    public void updateNews(News news)
    {
        em.merge(news);
    }
    
    public void deleteNews(News news)
    {
        em.remove(news);
    }
    
    public List<News> findAllNews()
    {
        TypedQuery<News> userList = em.createQuery("SELECT n FROM News n", News.class);
        return userList.getResultList();
    }
    
    public News findByID(Long id)
    {
        News result = new News();
        try
        {
            result = em.find(News.class, id);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return result;
    }
}
