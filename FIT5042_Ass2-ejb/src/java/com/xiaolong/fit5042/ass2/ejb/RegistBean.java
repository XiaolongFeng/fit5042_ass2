/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class RegistBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(Users usr) {
        em.persist(usr);
    }
    
    public void createUser(Users usr)
    {
        em.persist(usr);
    }
    
    public void updateUser(Users usr)
    {
        em.merge(usr);
    }
    
    public void deleteUser(Users usr)
    {
        em.remove(usr);
    }
    
    public List<Users> findAllUser()
    {
        TypedQuery<Users> userList = em.createQuery("SELECT u FROM User u", Users.class);
        return userList.getResultList();
    }
    
    public Users findByID(Long id)
    {
        Users result = new Users();
        result = em.find(Users.class, id);
        return result;
    }
}
