/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.OpenHours;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class OpenHourBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(OpenHours op)
    {
        em.persist(op);
    }
    
    public void create(OpenHours op)
    {
        em.persist(op);
    }
    
    public void update(OpenHours op)
    {
        em.merge(op);
    }
    
    public void delete(OpenHours op)
    {
        em.remove(op);
    }
    
    public List<OpenHours> findAllOpenHours()
    {
        TypedQuery<OpenHours> userList = em.createQuery("SELECT n FROM openHours n", OpenHours.class);
        return userList.getResultList();
    }

    
}
