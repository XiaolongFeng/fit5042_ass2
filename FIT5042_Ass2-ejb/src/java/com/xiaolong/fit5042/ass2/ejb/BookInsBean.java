/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.BookInstance;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class BookInsBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(BookInstance book) {
        em.persist(book);
    }
    
    public void createNews(BookInstance book)
    {
        em.persist(book);
    }

    public void updateNews(BookInstance book)
    {
        em.merge(book);
    }
    
    public void deleteNews(BookInstance book)
    {
        em.remove(em.merge(book));
    }
    
    public List<BookInstance> findAllNews()
    {
        TypedQuery<BookInstance> userList = em.createQuery("SELECT n FROM BookInstance n", BookInstance.class);
        return userList.getResultList();
    }
    
    public BookInstance findByID(Long id)
    {
        BookInstance result = new BookInstance();
        try{
            result = em.find(BookInstance.class, id);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return result;
    }
}
