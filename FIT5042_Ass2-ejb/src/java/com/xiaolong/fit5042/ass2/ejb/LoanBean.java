/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.Loan;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class LoanBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(Loan loan) {
        em.persist(loan);
    }
    
    public void createNews(Loan loan)
    {
        em.persist(loan);
    }

    public void updateNews(Loan loan)
    {
        em.merge(loan);
    }
    
    public void deleteNews(Loan loan)
    {
        em.remove(em.merge(loan));
    }
    
    public List<Loan> findAllNews()
    {
        TypedQuery<Loan> userList = em.createQuery("SELECT n FROM Loan n", Loan.class);
        return userList.getResultList();
    }
    
    public Loan findByID(Long id)
    {
        Loan result = new Loan();
        try{
            result = em.find(Loan.class, id);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return result;
    }
}
