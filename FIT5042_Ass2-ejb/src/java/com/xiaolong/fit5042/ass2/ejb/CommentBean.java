/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.Comments;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class CommentBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(Comments comment) {
        em.persist(comment);
    }
    
    public void createNews(Comments comment)
    {
        em.persist(comment);
    }

    public void updateNews(Comments comment)
    {
        em.merge(comment);
    }
    
    public void deleteNews(Comments comment)
    {
        em.remove(em.merge(comment));
    }
    
    public List<Comments> findAllNews()
    {
        TypedQuery<Comments> userList = em.createQuery("SELECT n FROM Comments n", Comments.class);
        return userList.getResultList();
    }
    
    public Comments findByID(Long id)
    {
        Comments result = new Comments();
        try{
            result = em.find(Comments.class, id);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return result;
    }
}
