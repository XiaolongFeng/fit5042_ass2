/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.ejb;

import com.xiaolong.fit5042.ass2.entities.Author;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author Xiaolong
 */
@Stateless
@LocalBean
public class AuthorBean {
    @PersistenceContext(unitName = "FIT5042_Ass2-ejbPU")
    private EntityManager em;

    public void persist(Author au) {
        em.persist(au);
    }
    
    public void createNews(Author au)
    {
        em.persist(au);
    }

    public void updateNews(Author au)
    {
        em.merge(au);
    }
    
    public void deleteNews(Author au)
    {
        em.remove(em.merge(au));
    }
    
    public List<Author> findAllNews()
    {
        TypedQuery<Author> userList = em.createQuery("SELECT n FROM Author n", Author.class);
        return userList.getResultList();
    }
    
    public Author findByID(Long id)
    {
        Author result = new Author();
        try{
            result = em.find(Author.class, id);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return result;
    }   
}
