/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.xiaolong.fit5042.ass2.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Xiaolong
 */
@Entity
public class Book implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = "BOOK_ID")
    private Long bookid;
    
    @NotNull
    private String title;
    @NotNull
    private String description;
    @NotNull
    private String publisher;
    @NotNull
    private String edition;
    @NotNull
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date publishYear;
    
    private String coverUrl;

    @ManyToMany (mappedBy = "books")
    private Collection<Category> cats;
    
    @ManyToMany (mappedBy = "books")
    private Collection<Author> authors;
    
    @OneToMany (mappedBy = "bookid")
    private Collection<BookInstance> bookins;
    
    @OneToMany (mappedBy = "bookid")
    private Collection<Comments> commentList;
    
    @OneToMany (mappedBy = "bookid")
    private Collection<BookMark> markList;
    
    

    public Long getId() {
        return bookid;
    }

    public void setId(Long id) {
        this.bookid = id;
    }

    public Long getBookid() {
        return bookid;
    }

    public void setBookid(Long bookid) {
        this.bookid = bookid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Date getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(Date publishYear) {
        this.publishYear = publishYear;
    }

    public Collection<Category> getCats() {
        return cats;
    }

    public void setCats(Collection<Category> cats) {
        this.cats = cats;
    }

    public Collection<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<Author> authors) {
        this.authors = authors;
    }

    public Collection<BookInstance> getBookins() {
        return bookins;
    }

    public void setBookins(Collection<BookInstance> bookins) {
        this.bookins = bookins;
    }

    public Collection<Comments> getCommentList() {
        return commentList;
    }

    public void setCommentList(Collection<Comments> commentList) {
        this.commentList = commentList;
    }

    public Collection<BookMark> getMarkList() {
        return markList;
    }

    public void setMarkList(Collection<BookMark> markList) {
        this.markList = markList;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookid != null ? bookid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.bookid == null && other.bookid != null) 
                || (this.bookid != null && !this.bookid.equals(other.bookid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.xiaolong.fit5042.ass2.Book[ id=" + bookid + " ]";
    }
    
}
